

cap program drop uall
program define uall
version 15

   qui which ua
   if _rc{
      dis as error "Warning: -uall- is supported by user command -ua-, auto downloading ..."
	  ssc install ua, replace
   }

   ua: unicode encoding set gb18030
   ua: unicode translate *
	 
end
