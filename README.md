> 主页：<https://www.lianxh.cn/>

### 1. ua 命令
适用于 stata15 用户。可以一次性对当前工作路径以及所有子文件夹中的文件进行转码(unicode)，以保证中文字符可以正常显示。

#### 安装
下载 `ua.ado` 和 `ua.hlp`，放置于 `D:\stata15\ado\base\u` 或 `D:\stata15\ado\plus\u` 文件夹中。

#### 使用
- 在 Stata 命令窗口中输入 `help ua`，查看命令介绍和 Stata 范例。参照范例使用即可。

#### Stata 范例
```stata

  * Change current working directory (CWD)
    . cd D:\stata15\ado\personal\mypaper

  * Unicode all .dta files in CWD and files in sub-directories
    . ua: unicode encoding set gb18030
    . ua: unicode translate *.dta

  * Unicode all files (.do, .ado, .dta, .hlp, etc.) in CWD and files in sub-directories
    . ua: unicode encoding set gb18030
    . ua: unicode translate *

```

---
### 2. `uall` 命令

#### Description

`uall` is shortcut for `help ua`.

It executes the following two commands:
```stata
  . ua: unicode encoding set gb18030
  . ua: unicode translate *
```

#### Syntax

- Short version (Chinese only):
```stata
     . uall
```
- Full version:
```stata
     . uall [encoding-name]
```
- encoding-name is the name of encoding, see the list of encodings.  
- The default is *gb18030* for Chinese. So, for Chinese user, you need not specify this option.



#### Examples
```stata
  * Change current working directory (CWD)
    . cd D:\stata15\ado\personal\mypaper

  * For Chinese users:
    . uall

  * For Japnese users:
    . uall ibm-943_P15A-2003
                
  * For Korean users:
    . uall ibm-970_P110_P110-2006_U2
```

#### Author

- Yujun,Lian (Arlion) Department of Finance, Lingnan College, Sun Yat-Sen University.
- E-mail: arlionn@163.com.
- 连玉君 ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn))    

  

&emsp;


--- - --



     
> [连享会](https://gitee.com/arlionn)-直播平台上线了！大家可以随时随地充电了！      
> &emsp;         
> <http://lianxh.duanshu.com>      
> &emsp;         
<img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">

> 长按二维码观看最新直播课。

- **D.** &#x1F535; [直播-空间计量全局模型及Matlab实现](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), 2020.2.20日，19:00-21:00，主讲：范巧。即将开播……  
- **C.** &#x1F34E; [视频-动态面板数据模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e), **随时在线观看**，主讲：连玉君。
- **B.** &#x1F34F; [视频-我的特斯拉-实证研究设计](https://mp.weixin.qq.com/s/NGwsr92_Vr1DGRbVqDVQIA)，**随时在线观看**，主讲：连玉君。     
- **A.** &#x1F36A; [直播-文本分析与爬虫专题](https://gitee.com/arlionn/Course/blob/master/Done/2020Text.md)，**2020.3.28-29， 4.4-5**，主讲：司继春，游万海。